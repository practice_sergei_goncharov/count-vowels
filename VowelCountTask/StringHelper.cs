﻿using System;

namespace VowelCountTask
{
#pragma warning disable
    public static class StringHelper
    {
        /// <summary>
        /// Calculates the count of vowels in the source string.
        ///  'a', 'e', 'i', 'o', and 'u' are vowels. 
        /// </summary>
        /// <param name="source">Source string.</param>
        /// <returns>Count of vowels in the given string.</returns>
        /// <exception cref="ArgumentException">Thrown when source string is null or empty.</exception>
        public static int GetCountOfVowel(string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentException("Source string is null or empty.");
            }

            int vowelCount = 0;
            for (int i = 0; i < source.Length; i++)
            {
                char currentChar = char.ToLower(source[i]);
                if (currentChar == 'a' || currentChar == 'e' || currentChar == 'i' || currentChar == 'o' || currentChar == 'u')
                {
                    vowelCount++;
                }
            }

            return vowelCount;
        }
    }
}
